const bg = document.querySelector("body");
const authorEl = document.querySelector("#author");
const cryptoEl = document.querySelector(".crypto-container");
const weatherEl = document.querySelector(".weather");
const weatherApi = config.OPENWEATHER;

function getBGImage() {
  fetch(
    "https://apis.scrimba.com/unsplash/photos/random?orientation=landscape&query=mountains"
  )
    .then((res) => res.json())
    .then((img) => {
      bg.style.backgroundImage = `url(${img.urls.regular})`;
      authorEl.textContent = `By ${img.user.name}`;
    })
    .catch((err) => {
      document.body.style.backgroundImage = `url(https://images.unsplash.com/photo-1497436072909-60f360e1d4b1?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMTEwMjl8MHwxfHJhbmRvbXx8fHx8fHx8fDE2MjI4NDE2NzA&ixlib=rb-1.2.1&q=80&w=1080)`;
      //console.log(err)
    });
}

function getCoin(coin) {
  fetch(`https://api.coingecko.com/api/v3/coins/${coin}`)
    .then((res) => {
      if (!res.ok) {
        throw Error("There was an error");
      }
      return res.json();
    })
    .then((data) => {
      return renderCoin(data);
    })
    .catch((err) => alert(err));
}

function renderCoin(data) {
  cryptoEl.innerHTML += `
    <div class="card">
        <div class="crypto">
            <img id="coin-ico" src="${data.image.small}" />
            <p id="coin-name">${data.name} - in CDN</p>
        </div>
        <div class="crypto-info">
                <p class="coin-value-ico">🎯 Current Price: ${data.market_data.current_price.cad}</p>
                <p class="coin-value-ico">👆 24h High Price: ${data.market_data.high_24h.cad}</p>
                <p class="coin-value-ico">👇 24h Low Price: ${data.market_data.low_24h.cad}</p>
            </div>
    </card>
        `;
}

function getWeather(lat, lon) {
  fetch(
    `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&units=metric&appid=${weatherApi}`
  )
    .then((res) => {
      if (!res.ok) {
        throw Error("Weather Api isn't working" + res.status);
      }
      return res.json();
    })
    .then((data) => {
      console.log(data);
      return renderWeather(data);
    })
    .catch((err) => console.log(err));
}

function getGeoLocation() {
  navigator.geolocation.getCurrentPosition((position) => {
    getWeather(position.coords.latitude, position.coords.longitude);
  });
}

function renderWeather(data) {
  weatherEl.innerHTML = `
    <div class="weather">
        <div class="weather-card-top">                     
            <img id="weather-icon" src="http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png"/>
            <p id="temp">${Math.round(data.main.temp)}° </p>
        </div>
        <p id="weather-city">${data.name}</p>
    </div>                          
    `;
}

function getTime() {
    const time = new Date();
    document.querySelector("#time").textContent = time.toLocaleTimeString(
      "en-US",
      { timeStyle: "short" }
    );
  }
  

// start App
 function startApp() {
    getBGImage();
    getCoin("bitcoin");
    getCoin("shiba-inu");
    getCoin("dogecoin");
    getGeoLocation();
    setInterval(getTime, 1000);
 }

 startApp();


